# CLINICAL DATASET

 The study was conducted at XXX. It consisted of 5 patients (4 completed).  Hourly step counts, heart rates and calories burned were collected from 10 AM to 7 PM over 60 days via Microsoft Band 2.  Daily and weekly Patient Reported Outcomes (PRO) data was collected over the course of 60 days on device compliance and other measures using smartphone apps.

## STUDY TIMELINE

```{}
BStart                              BendEnd                  Follow-up
   |      |      |       |      |     |         ...            |

<-----         60d Band + PRO    -----><--- 90d follow-up ---->
```

- `BStart`: day Band data collection starts (date of "screening CRF")

- `BEnd`: day Band data collection must stop (on day 60 of study)

- `Follow-up`: day of follow-up (on day 150 of study)

## DATASET

The dataset includes the following files.

__SUMMARY DATA FILES__: contain records for *all* subjects.

- `PRO_SCORES.csv`: PRO scores

__PER SUBJECT DATA FILES__: contain records per subject in stored in folders named `P###` where the 3 digits correspond to the subject id in range [001-006]. Note that only folders with data are included in this dataset and not all folders have completed the study (have partial data); refer to the status table for completion status.

Files include：

- Band data

## SUBJECTS

- 5 subjects were included and 4 completed the study. 
- Data for the 1 subjects that dropped-out is included.

| Status        | Count | Id    |
| ------------- | ----- | ----- |
| Dropped-out   | 1     | P005  |
| Complete      | 4     | P001 P002 P003 P004  |

## DATA FILES DETAILS

### BAND DATA

The timestamps in MS Band data file were converted to local time for ease of analysis; there are one-hour time shifts when Daylight Saving Time (DST):

- 2016 DST ended (clock moved backward) on Sunday, November 6, 2016 at 2:00 am
- 2017 DST started (clock moved forward) on Sunday, March 12, 2017 at 2:00 am

```{text}
 id: subject id e.g., P001
 start_time: start time of the summary
 end_time: end time of the summary
 period: period of the summary (1: daily, 2: hourly)
 steps: total number of steps taken in period 
 active_hours: number of active hours in period, used for Daily summary
 total_calories: total calories burned in the period
 average_heart_rate: average heart rate during the period
 peek_heart_rate: peak heart rate during the period
 lowest_heart_rate: lowest heart rate during the period
 total_distance: total distance in the period
 total_distance_on_foot: total distance covered on foot
 actual_distance: absolute distance, including any paused time
 elevation_gain: cumulative elevation gain during the period
 elevation_loss: cumulative elevation loss during the period
 max_elevation: maximum elevation during the period
 min_elevation: minimum elevation during the period
 way_point_distance: distance in cm used to waypoint the GPS data
 speed: total Period distance divided by period duration
 pace: period duration divided by total period distance
 overall_pace: duration of all periods divided by distance of all periods
```

### PRO

PRO data include `PROMIS T-scores` for all subjects in `PRO_SCORES.csv`.

```{}
id: subject id e.g., P001
day: study day corresponding to the measurement
item:
    PROMIS_SOCIAL_ISOLATION: PROMIS Social Isolation Scale score.
    PROMIS_SLEEP_DISTURBANCE: PROMIS Sleep Disturbance Scale score.
    PROMIS_PHYSICAL_FUNCTION: Physical Function Scale score.
    PROMIS_WEEKLY_FATIGUE: Weekly Fatigue Scale score.
    PROMIS_DAILY_FATIGUE: Daily Fatigue Scale score.
    SDS001: Appetite item:
    Weight: weight recorded on study scale (lbs)
score: PROMIS scale scores or measurement (e.g., weight)
timestamp: timestamp of measurement, last measurement in the case of multiple items.
```
